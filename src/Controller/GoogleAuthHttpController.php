<?php

namespace Drupal\social_auth_google_api\Controller;

use Drupal\social_auth_decoupled\SocialAuthDecoupledTrait;
use Drupal\social_auth_decoupled\SocialAuthHttpInterface;
use Drupal\social_auth_google\Controller\GoogleAuthController;

/**
 * Post login responses for Social Auth Google.
 */
class GoogleAuthHttpController extends GoogleAuthController implements SocialAuthHttpInterface {

  use SocialAuthDecoupledTrait;

  /**
   * {@inheritdoc}
   */
  public function authenticateUserByProfile($profile, $data) {
    return $this->userAuthenticatorHttp()
      ->authenticateUser($profile->getName(), $profile->getEmail(), $profile->getId(), $this->providerManager->getAccessToken(), $profile->getAvatar(), $data);
  }

}
