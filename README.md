# Social auth google api

This module provide a endpoint to login to drupal by google's access_token

### Endpoint
#### Url:
`/user/login/google?_format=json` [POST]
#### Post body
```
{
    "access_token":"ya29.ImC2B-aVVZQJfY5H6B7Yg61Pf5JSh70tKO_GfFg3AHumWSLy9demMM7
rYLfZ7GeMdRO6kDWdWQNh7yJ4IAcmVOxaZN7OHs14REe9V0Y0_bNNN4wGhmiPl_vo_cxQJ0Rej2Q"
}
```
